package ru.sbertask.dictionary.presentation

/**
 * Базовые интерфейсы для View и Presenter (для MVP архитектуры ).
 *
 * Created by Andrey on 06.11.2019
 */
class BaseContract {
    interface Presenter<in T>{
        fun attach(view: T)
        fun dropView();
    }

    interface View{
    }
}