package ru.sbertask.dictionary.presentation.mainscreen

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.sbertask.dictionary.data.room.entity.TranslateRecord
import ru.sbertask.dictionary.domain.interactor.MainScreenInteractor

/**
 * Презентер для основного экрана(View) приложения
 *
 * Created by Andrey on 06.11.2019
 */
class MainPresenter(private val mainScreenInteractor: MainScreenInteractor) :
    MainScreenContract.Presenter {

    private val disposable = CompositeDisposable()

    private var view: MainScreenContract.View? = null


    override fun attach(view: MainScreenContract.View) {
        this.view = view;
    }

    override fun dropView() {
        this.view = null
        disposable.dispose()
    }

    override fun initList() {
        disposable.add(
            mainScreenInteractor.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { onGetAllSuccess(it) },
                    { handleError(it) })
        )
    }

    override fun getWord(word: String, langFrom: String, langTo: String) {
        disposable.add(
            mainScreenInteractor.getTranslateRecord(word, langFrom, langTo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { onGetWordSuccess(it) },
                    { handleError(it) })
        )
    }

    override fun findWord(word: String) {
        disposable.add(
            mainScreenInteractor.findWord(word)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onFindWordSuccess(it) },
                    { handleError(it) })
        )
    }

    fun onGetAllSuccess(items: List<TranslateRecord>) {
        view?.showWordList(items)
    }

    fun onGetWordSuccess(translateRecord: TranslateRecord) {
        view?.showTranslatedWord(translateRecord.wordTo)
        initList()
    }

    fun onFindWordSuccess(items: List<TranslateRecord>) {
        view?.showWordList(items)
    }

    fun handleError(error: Throwable) {
        view?.showError(error.localizedMessage)
    }
}