package ru.sbertask.dictionary.presentation.mainscreen

import ru.sbertask.dictionary.data.room.entity.TranslateRecord
import ru.sbertask.dictionary.presentation.BaseContract

/**
 * Type description here....
 *
 * Created by Andrey on 06.11.2019
 */
class MainScreenContract {
    interface View : BaseContract.View{
        fun showTranslatedWord(word : String)
        fun showWordList(items : List<TranslateRecord>)
        fun showError(error : String)
    }

    interface Presenter: BaseContract.Presenter<MainScreenContract.View>{
        fun initList()
        fun getWord(word: String, langFrom: String, langTo: String)
        fun findWord(word: String)
    }
}