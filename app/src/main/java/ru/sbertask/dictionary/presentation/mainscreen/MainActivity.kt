package ru.sbertask.dictionary.presentation.mainscreen

import android.os.Bundle
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import ru.sbertask.dictionary.DictionaryApp
import ru.sbertask.dictionary.R
import ru.sbertask.dictionary.data.room.entity.TranslateRecord
import javax.inject.Inject

/**
 * Основной экран приложения
 */
class MainActivity : AppCompatActivity(), MainScreenContract.View {

    @Inject
    lateinit var presenter: MainScreenContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        injectDependencies()
        initListeners()
        presenter.attach(this)
        presenter.initList()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchItem = menu?.findItem(R.id.action_search)
        val searchView = searchItem?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String): Boolean {
                presenter.findWord(query)
                return true
            }
            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })

        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                presenter.initList()
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun showTranslatedWord(word: String) {
        transToEditText.setText(word)
    }

    override fun showWordList(items: List<TranslateRecord>) {
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = TranslateListAdapter(items)
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    private fun initListeners() {
        transFromEditText.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                presenter.getWord(
                    transFromEditText.text.toString(),
                    langFromSpinner.selectedItem.toString(),
                    langToSpinner.selectedItem.toString())
                true
            } else {
                false
            }
        }
    }

    private fun injectDependencies() {
        DictionaryApp
            .appComponent
            .addMainScreenComponent()
            .inject(this);
    }
}
