package ru.sbertask.dictionary.presentation.mainscreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_translation.view.*
import ru.sbertask.dictionary.R
import ru.sbertask.dictionary.data.room.entity.TranslateRecord

/**
 * Адаптер для списка переводов
 *
 * Created by Andrey on 06.11.2019
 */
class TranslateListAdapter(private val mItems: List<TranslateRecord>) :
    RecyclerView.Adapter<TranslateListAdapter.ItemHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.item_translation, parent, false)
        return ItemHolder(v)
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bind(mItems[position])
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    class ItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mFromTextView: TextView
        val mToTextView: TextView
        val mDirectionTextView: TextView

        init {
            mFromTextView = itemView.fromTextView
            mToTextView = itemView.toTextView
            mDirectionTextView = itemView.directionTextView
        }

        fun bind(record: TranslateRecord) {
            mFromTextView.text = record.wordFrom
            mToTextView.text = record.wordTo
            mDirectionTextView.text = record.direction
        }
    }
}