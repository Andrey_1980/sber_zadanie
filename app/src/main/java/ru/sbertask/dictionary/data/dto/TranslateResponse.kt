package ru.sbertask.dictionary.data.dto

/**
 * Ответ сервера на запрос на перевод
 *
 * Created by Andrey on 06.11.2019
 */
data class TranslateResponse(
    val code : Int,
    val lang : String,
    val text : List<String>
)