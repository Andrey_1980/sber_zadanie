package ru.sbertask.dictionary.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.sbertask.dictionary.data.room.dao.TranslateRecordDao
import ru.sbertask.dictionary.data.room.entity.TranslateRecord

/**
 * Класс для работы с ORM Room
 *
 * Created by Andrey on 06.11.2019
 */
@Database(entities = arrayOf(TranslateRecord::class), version = 1, exportSchema = false)
abstract class TranslateRoomDatabase : RoomDatabase() {
    abstract fun getTranslateRecordDao() : TranslateRecordDao
}