package ru.sbertask.dictionary.data.repository

import io.reactivex.Single
import ru.sbertask.dictionary.data.dto.TranslateResponse
import ru.sbertask.dictionary.data.network.RetrofitService

/**
 * Репозитоий для работы с сервисом переводя Яндекса
 *
 * Created by Andrey on 06.11.2019
 */
class YandexRepository(private val retrofitService: RetrofitService) {

    fun getTranslateResponse(direction: String, wordFrom: String): Single<TranslateResponse> =
        retrofitService.getTranslate(wordFrom, direction)

}