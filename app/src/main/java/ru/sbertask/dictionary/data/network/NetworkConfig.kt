package ru.sbertask.dictionary.data.network

/**
 * Настройки для сервиса перевода Яндекс
 *
 * Created by Andrey on 06.11.2019
 */
class NetworkConfig {
    companion object {
        const val BASE_URL = "https://translate.yandex.net"
        const val API_KEY  = "trnsl.1.1.20130721T071649Z.09dc70bd1d55c114.88b4816f1eba123df163ce2678971ac3727f21e9"
    }
}