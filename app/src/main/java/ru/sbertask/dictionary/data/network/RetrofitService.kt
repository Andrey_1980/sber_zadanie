package ru.sbertask.dictionary.data.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.sbertask.dictionary.data.dto.TranslateResponse

/**
 * Type description here....
 *
 * Created by Andrey on 06.11.2019
 */
interface RetrofitService {

    @GET("/api/v1.5/tr.json/translate?key=" + NetworkConfig.API_KEY)
    fun getTranslate(@Query("text") text : String,
                     @Query("lang") lang : String) : Single<TranslateResponse>

}