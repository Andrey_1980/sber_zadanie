package ru.sbertask.dictionary.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import ru.sbertask.dictionary.data.room.entity.TranslateRecord

/**
 * Основные методы получения\изменения данных
 *
 * Created by Andrey on 06.11.2019
 */
@Dao
interface TranslateRecordDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(record : TranslateRecord) : Completable

    @Query("SELECT * FROM TranslateRecord WHERE direction = :direction AND wordFrom = :wordFrom ")
    fun getTranslateRecord(direction : String, wordFrom : String) : Single<TranslateRecord>

    @Query("SELECT * FROM TranslateRecord")
    fun getAll() : Single<List<TranslateRecord>>

    @Query("SELECT * FROM TranslateRecord WHERE wordFrom = :word OR wordTo = :word")
    fun findByWord(word : String) : Single<List<TranslateRecord>>
}
