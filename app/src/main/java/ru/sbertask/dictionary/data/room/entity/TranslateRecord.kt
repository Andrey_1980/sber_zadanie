package ru.sbertask.dictionary.data.room.entity

import androidx.room.Entity
import androidx.room.Index

/**
 * Сущность - запись одного перевода
 *
 * Created by Andrey on 06.11.2019
 */
@Entity(primaryKeys = arrayOf("direction", "wordFrom"),indices = arrayOf(Index(value = ["wordTo"])))
data class TranslateRecord(
    val direction : String,
    val wordFrom : String,
    val wordTo : String
)