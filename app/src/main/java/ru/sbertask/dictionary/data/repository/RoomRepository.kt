package ru.sbertask.dictionary.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import ru.sbertask.dictionary.data.room.dao.TranslateRecordDao
import ru.sbertask.dictionary.data.room.entity.TranslateRecord

/**
 * Репозитоий для работы с локальным хранилещем
 *
 * Created by Andrey on 06.11.2019
 */
class RoomRepository(private val translateRecordDao: TranslateRecordDao) {


    fun getAll(): Single<List<TranslateRecord>> =
        translateRecordDao.getAll()

    fun findWord(word: String): Single<List<TranslateRecord>> =
        translateRecordDao.findByWord(word)

    fun getTranslateRecord(direction: String, wordFrom: String): Single<TranslateRecord> =
        translateRecordDao.getTranslateRecord(direction, wordFrom)


    fun storeTranslateRecord(translateRecord: TranslateRecord): Completable =
        translateRecordDao.insert(translateRecord)

}