package ru.sbertask.dictionary.dagger.mainscreen

import dagger.Subcomponent
import ru.sbertask.dictionary.presentation.mainscreen.MainActivity

/**
 * Компонент основного экрана приложения
 *
 * Created by Andrey on 06.11.2019
 */
@Subcomponent(modules = arrayOf(MainScreenModule::class))
@MainScreenScope
interface MainScreenComponent {
    fun inject(activity: MainActivity)
}