package ru.sbertask.dictionary.dagger.mainscreen

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Scope

/**
 * Type description here....
 *
 * Created by Andrey on 06.11.2019
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class MainScreenScope