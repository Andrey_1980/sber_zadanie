package ru.sbertask.dictionary.dagger

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ru.sbertask.dictionary.data.room.TranslateRoomDatabase
import ru.sbertask.dictionary.data.room.dao.TranslateRecordDao
import javax.inject.Singleton

/**
 * Модуль для предостовления сервиса работы с локальнвм хранилищем
 *
 * Created by Andrey on 06.11.2019
 */
@Module
class RoomModule {
    @Singleton
    @Provides
    fun provideRoomDatabase(context : Context) : TranslateRoomDatabase =
        Room.databaseBuilder(context, TranslateRoomDatabase::class.java, "translations" ).build()

    @Singleton
    @Provides
    fun provideTranslateRecordDao(translateRoomDatabase: TranslateRoomDatabase) : TranslateRecordDao =
        translateRoomDatabase.getTranslateRecordDao()
}