package ru.sbertask.dictionary.dagger

import dagger.Component
import ru.sbertask.dictionary.dagger.mainscreen.MainScreenComponent
import ru.sbertask.dictionary.data.network.RetrofitService
import ru.sbertask.dictionary.data.room.TranslateRoomDatabase
import ru.sbertask.dictionary.data.room.dao.TranslateRecordDao
import javax.inject.Singleton

/**
 * Основной компонент приложения
 *
 * Created by Andrey on 06.11.2019
 */
@Component(modules = arrayOf(AppModule::class, NetworkModule::class, RoomModule::class))
@Singleton
interface AppComponent {

    fun addMainScreenComponent(): MainScreenComponent

    fun provideRetrofitService(): RetrofitService
    fun provideTranslateRecordDao(): TranslateRecordDao
    fun provideTranslateRoomDatabase() : TranslateRoomDatabase
}