package ru.sbertask.dictionary.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Модуль для предоставления контекста зависимостям
 *
 * Created by Andrey on 06.11.2019
 */
@Module
class AppModule(val context : Context) {
    @Singleton
    @Provides
    fun provideContext() : Context = context
}