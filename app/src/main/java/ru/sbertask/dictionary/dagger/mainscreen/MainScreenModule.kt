package ru.sbertask.dictionary.dagger.mainscreen

import dagger.Module
import dagger.Provides
import ru.sbertask.dictionary.data.network.RetrofitService
import ru.sbertask.dictionary.data.repository.RoomRepository
import ru.sbertask.dictionary.data.repository.YandexRepository
import ru.sbertask.dictionary.data.room.dao.TranslateRecordDao
import ru.sbertask.dictionary.domain.interactor.MainScreenInteractor
import ru.sbertask.dictionary.presentation.mainscreen.MainPresenter
import ru.sbertask.dictionary.presentation.mainscreen.MainScreenContract

/**
 * Модуль для предоставления зависимостей основному экрану приложения
 *
 * Created by Andrey on 06.11.2019
 */
@Module
class MainScreenModule {

    @MainScreenScope
    @Provides
    fun provideMainScreenPresenter(mainScreenInteractor: MainScreenInteractor) : MainScreenContract.Presenter =
         MainPresenter(mainScreenInteractor)

    @MainScreenScope
    @Provides
    fun provideMainScreenInteractor(roomRepository: RoomRepository,
                                    yandexRepository: YandexRepository):MainScreenInteractor =
        MainScreenInteractor(roomRepository, yandexRepository)

    @MainScreenScope
    @Provides
    fun provideRoomRepository(translateRecordDao: TranslateRecordDao) : RoomRepository =
        RoomRepository(translateRecordDao)

    @MainScreenScope
    @Provides
    fun provideYandexRepository(retrofitService: RetrofitService) : YandexRepository =
        YandexRepository(retrofitService)
}