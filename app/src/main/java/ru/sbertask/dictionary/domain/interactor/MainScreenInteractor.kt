package ru.sbertask.dictionary.domain.interactor

import android.util.Log
import androidx.room.util.StringUtil
import io.reactivex.Single
import ru.sbertask.dictionary.data.repository.RoomRepository
import ru.sbertask.dictionary.data.repository.YandexRepository
import ru.sbertask.dictionary.data.room.entity.TranslateRecord

/**
 * Основной интерактор
 *
 * Created by Andrey on 06.11.2019
 */
class MainScreenInteractor(
    private val roomRepository: RoomRepository,
    private val yandexRepository: YandexRepository
) {
    /**
     * Выбрать все записи
     */
    fun getAll(): Single<List<TranslateRecord>> =
        roomRepository.getAll()

    /**
     * Искать слово и по оригиналам и по переводам
     */
    fun findWord(word: String): Single<List<TranslateRecord>> =
        roomRepository.findWord(word)

    /**
     * Запросить слово в локальной базе, в случае ошибки(отсутствия) - запросить с яндекса
     */
    fun getTranslateRecord(word: String,langFrom: String,langTo: String): Single<TranslateRecord> {
        val direction = langFrom.toLowerCase() + "-" + langTo.toLowerCase()
        return roomRepository.getTranslateRecord(direction, word)
            .onErrorResumeNext(yandexRepository.getTranslateResponse(direction, word)
                .map {
                    TranslateRecord(
                        it.lang,
                        word,
                        // Для упрощения берем первый вариант в случаях нескольких переводов
                        if (it.text.isEmpty()) "" else it.text.get(0)
                    )
                }
                .flatMap {
                    this.saveTranslateRecord(it)
                })
    }

    /**
     * Сохранение нового перевода в локальной базе
     */
    private fun saveTranslateRecord(translateRecord: TranslateRecord): Single<TranslateRecord> =
        roomRepository.storeTranslateRecord(translateRecord).toSingle { translateRecord }
}