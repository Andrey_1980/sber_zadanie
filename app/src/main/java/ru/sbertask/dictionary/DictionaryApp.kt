package ru.sbertask.dictionary

import android.app.Application
import ru.sbertask.dictionary.dagger.*

/**
 * Основной класс приложения
 *
 * Created by Andrey on 06.11.2019
 */
class DictionaryApp : Application(){
    companion object {
        lateinit var appComponent : AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    fun initDagger() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule())
            .roomModule(RoomModule())
            .build()
    }
}